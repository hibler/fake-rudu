"""Fake RU and DU multiple vlan, PTP and SyncE test
"""

#
# Semi-simulate an experiment with two RUs attached to a DU. Requirements:
#
# - Each RU should be connected to the DU with its own physical link.
#
# - Each such link should have both an untagged VLAN for management and a
#   tagged VLAN for the Xhaul.
#
# - Xhaul VLAN tags are fixed in the srsRAN config on the RU. If you change
#   the tags here, you will have to change them on the RUs. Just don't!
#
# - The RU side switch interfaces should have PTP and SyncE enabled on the
#   the interface.
#
# - The RUs have an IP address hardwired on the management vlan and it is
#   the _same_ address on both. Hence we do not assign an IP address to
#   these interfaces and let the user configure one at a time as needed.
#   XXX for now, since we must assign an IP, we give them different IP
#   subnets. Hopefully this will not confuse the RUs.
#
# - The DU should have a PTP-enabled link. We use the node side of one of
#   the RU interfaces and use a startup script to install/configure the
#   Linux PTP software.
#

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as pg
# Import IG extensions
import geni.rspec.igext as ig
# Emulab specific extensions.
import geni.rspec.emulab as emulab
# Spectrum specific extensions.
import geni.rspec.emulab.lanext as lanext

# Create a portal context, needed to defined parameters
pc = portal.Context()

# Create a Request object to start building the RSpec.
request = pc.makeRequestRSpec()

## Debugging
#request.skipVlans()

dutype = "d740"
rutype = "d740"
duimage = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU22-64-STD"

# See if DU node type supports PTP.
# It should, but I want to be able to use other node types to debug VLAN stuff.
doptp = 0
if dutype == "d740" or dutype == "d840":
    doptp = 1
    pass

# Fake DU node
du = request.RawPC("du")
du.hardware_type = dutype
du.disk_image = duimage
# RU-1 untagged management vlan, fake IP, includes PTP
duru1mgmt = du.addInterface()
duru1mgmt.component_id = "eth4"
duru1mgmt.addAddress(pg.IPv4Address("10.13.1.1", "255.255.255.0"))
if doptp:
    duru1mgmt.PTP()
    du.addService(pg.Execute(shell="sh", command="sudo sh /local/repository/scripts/startup.sh"))
    pass
# RU-1 tagged xhaul vlan, shares same physical IF as mgmt
duru1xhaul = du.addInterface()
duru1xhaul.component_id = "eth4"
# RU-2 untagged management vlan, fake IP, no PTP
duru2mgmt = du.addInterface()
duru2mgmt.component_id = "eth5"
duru2mgmt.addAddress(pg.IPv4Address("10.13.2.1", "255.255.255.0"))
# RU-2 tagged xhaul vlan, shares same physical IF as mgmt
duru2xhaul = du.addInterface()
duru2xhaul.component_id = "eth5"

# See if fake RU node type supports PTP.
# It should, but I want to be able to use other node types to debug VLAN stuff.
doptp = 0
if rutype == "d740" or rutype == "d840":
    doptp = 1
    pass

# Fake RU node 1
ru1 = request.RawPC("ru1")
ru1.hardware_type = rutype
# RU-1 untagged management vlan, enable PTP and SyncE
ru1mgmt = ru1.addInterface()
ru1mgmt.component_id = "eth4"
# XXX does not matter, just to keep genilib happy
ru1mgmt.addAddress(pg.IPv4Address("10.13.1.2", "255.255.255.0"))
if doptp:
    ru1mgmt.PTP()
    ru1.addService(pg.Execute(shell="sh", command="sudo sh /local/repository/scripts/startup.sh"))
    ru1mgmt.SyncE()
    # XXX note that there is no node-side SyncE support
    pass
# RU-1 tagged xhaul vlan
ru1xhaul = ru1.addInterface()
ru1xhaul.component_id = "eth4"

# Fake RU node 2
ru2 = request.RawPC("ru2")
ru2.hardware_type = rutype
# RU-2 untagged management vlan, enable PTP and SyncE
ru2mgmt = ru2.addInterface()
ru2mgmt.component_id = "eth4"
# XXX does not matter, just to keep genilib happy
ru2mgmt.addAddress(pg.IPv4Address("10.13.2.2", "255.255.255.0"))
if doptp:
    ru2mgmt.PTP()
    ru2.addService(pg.Execute(shell="sh", command="sudo sh /local/repository/scripts/startup.sh"))
    ru2mgmt.SyncE()
    # XXX note that there is no node-side SyncE support
    pass
# RU-2 tagged xhaul vlan
ru2xhaul = ru2.addInterface()
ru2xhaul.component_id = "eth4"

# Link DU untagged/tagged vlans with RU

# Connect untagged RU-1 mgmt
duru1u = request.Link("duru1u", members = [duru1mgmt, ru1mgmt])
duru1u.DualModeTrunking()
# Connect tagged RU-1 xhaul
duru1t = request.Link("duru1t", members = [duru1xhaul, ru1xhaul])
duru1t.setVlanTag(23)
duru1t.DualModeTrunking(duru1u)

# Connect untagged RU-2 mgmt
duru2u = request.Link("duru2u", members = [duru2mgmt, ru2mgmt])
duru2u.DualModeTrunking()
# Connect tagged RU-2 xhaul
duru2t = request.Link("duru2t", members = [duru2xhaul, ru2xhaul])
duru2t.setVlanTag(24)
duru2t.DualModeTrunking(duru2u)

# Print the RSpec to the enclosing page.
pc.printRequestRSpec(request)
