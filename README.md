Profile to configure a Benetel RU experiment without actual RUs.

This profile is used for debugging the multiple-vlan-per-interface
and PTP/SyncE support in `snmpit`.
